//No parameters
procedure TMap.MapEggre(out Floor, Wall: TMapType);
var
  BiomeFloor, BiomeWall: Integer;
  x1, y1, x2, y2: Integer;
  xi, yi: Integer;
  i: Integer;
  v: Integer;
begin
  Log(CurrentRoutine, 'Generating map...');
  BiomeFloor := RandomFloorBiome;
  BiomeWall := RandomWallBiome;

  Floor := FillMap(SizeX, SizeY, BiomeFloor);
  Wall := FillMap(SizeX, SizeY, BiomeWall);

  x1 := 0;
  y1 := 0;
  x2 := Pred(SizeX);
  y2 := Pred(SizeY);
  v := -1;
  repeat
    MapRectangle(Wall, x1, y1, x2, y2, v);

    //flip phase
    if v = -1 then
      v := BiomeWall
    else
      v := -1;

    //make passages or blocking walls
    if (y2-y1 > 7) and (x2-x1 > 7) then
    begin
      for i := 0 to RND.Random((y2 - y1) div 10) do
      begin
        yi := RND.Random(y2 - y1 - 4) + y1 + 2;
        MapLine(Wall, x1, yi, x1+5, yi, v);
      end;
      for i := 0 to RND.Random((y2 - y1) div 10) do
      begin
        yi := RND.Random(y2 - y1 - 4) + y1 + 2;
        MapLine(Wall, x2-5, yi, x2, yi, v);
      end;
      for i := 0 to RND.Random((x2 - x1) div 10) do
      begin
        xi := RND.Random(x2 - x1 - 4) + x1 + 2;
        MapLine(Wall, xi, y1, xi, y1+5, v);
      end;
      for i := 0 to RND.Random((x2 - x1) div 10) do
      begin
        xi := RND.Random(x2 - x1 - 4) + x1 + 2;
        MapLine(Wall, xi, y2-5, xi, y2, v);
      end;
    end;

    x1 += 1 + RND.Random(4);
    y1 += 1 + RND.Random(4);
    x2 -= 1 + RND.Random(4);
    y2 -= 1 + RND.Random(4);
  until (x2 <= x1) or (y2 <= y1);
end;

