{ Copyright (C) 2018 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ --------------------------------------------------------------------------- }

(* {} *)

unit SpritesUnit;

{$INCLUDE compilerconfig.inc}

interface

uses
  CastleGLImages;

const
  MaxWalls = 4;
  MaxFloors = 2;

var
  WallSprites: array[0..MaxWalls] of array of TGlImage;
  FloorSprites: array[0..MaxFloors] of array of TGlImage;

function RandomFloorBiome: Integer;
function RandomWallBiome: Integer;
procedure LoadSprites;
procedure FreeSprites;
implementation
uses
  Global,
  CastleFilesUtils,
  DecoLog;

procedure LoadSprites;
begin
  Log(CurrentRoutine, 'Loading sprites');
  SetLength(WallSprites[0], 5);
  WallSprites[0][0] := TGLImage.Create(ApplicationData('walls/church_0.png'));
  WallSprites[0][1] := TGLImage.Create(ApplicationData('walls/church_1.png'));
  WallSprites[0][2] := TGLImage.Create(ApplicationData('walls/church_2.png'));
  WallSprites[0][3] := TGLImage.Create(ApplicationData('walls/church_3.png'));
  WallSprites[0][4] := TGLImage.Create(ApplicationData('walls/church_4.png'));
  SetLength(WallSprites[1], 6);
  WallSprites[1][0] := TGLImage.Create(ApplicationData('walls/crystal_wall_1_0.png'));
  WallSprites[1][1] := TGLImage.Create(ApplicationData('walls/crystal_wall_6.png'));
  WallSprites[1][2] := TGLImage.Create(ApplicationData('walls/crystal_wall_7.png'));
  WallSprites[1][3] := TGLImage.Create(ApplicationData('walls/crystal_wall_8.png'));
  WallSprites[1][4] := TGLImage.Create(ApplicationData('walls/crystal_wall_9.png'));
  WallSprites[1][5] := TGLImage.Create(ApplicationData('walls/crystal_wall_11.png'));
  SetLength(WallSprites[2], 7);
  WallSprites[2][0] := TGLImage.Create(ApplicationData('walls/wall_vines_0.png'));
  WallSprites[2][1] := TGLImage.Create(ApplicationData('walls/wall_vines_1.png'));
  WallSprites[2][2] := TGLImage.Create(ApplicationData('walls/wall_vines_2.png'));
  WallSprites[2][3] := TGLImage.Create(ApplicationData('walls/wall_vines_3.png'));
  WallSprites[2][4] := TGLImage.Create(ApplicationData('walls/wall_vines_4.png'));
  WallSprites[2][5] := TGLImage.Create(ApplicationData('walls/wall_vines_5.png'));
  WallSprites[2][6] := TGLImage.Create(ApplicationData('walls/wall_vines_6.png'));
  SetLength(WallSprites[3], 12);
  WallSprites[3][0] := TGLImage.Create(ApplicationData('walls/stone_brick_1.png'));
  WallSprites[3][1] := TGLImage.Create(ApplicationData('walls/stone_brick_2.png'));
  WallSprites[3][2] := TGLImage.Create(ApplicationData('walls/stone_brick_3.png'));
  WallSprites[3][3] := TGLImage.Create(ApplicationData('walls/stone_brick_4.png'));
  WallSprites[3][4] := TGLImage.Create(ApplicationData('walls/stone_brick_5.png'));
  WallSprites[3][5] := TGLImage.Create(ApplicationData('walls/stone_brick_6.png'));
  WallSprites[3][6] := TGLImage.Create(ApplicationData('walls/stone_brick_7.png'));
  WallSprites[3][7] := TGLImage.Create(ApplicationData('walls/stone_brick_8.png'));
  WallSprites[3][8] := TGLImage.Create(ApplicationData('walls/stone_brick_9.png'));
  WallSprites[3][9] := TGLImage.Create(ApplicationData('walls/stone_brick_10.png'));
  WallSprites[3][10] := TGLImage.Create(ApplicationData('walls/stone_brick_11.png'));
  WallSprites[3][11] := TGLImage.Create(ApplicationData('walls/stone_brick_12.png'));
  SetLength(WallSprites[4], 10);
  WallSprites[4][0] := TGLImage.Create(ApplicationData('walls/sandstone_wall_0.png'));
  WallSprites[4][1] := TGLImage.Create(ApplicationData('walls/sandstone_wall_1.png'));
  WallSprites[4][2] := TGLImage.Create(ApplicationData('walls/sandstone_wall_2.png'));
  WallSprites[4][3] := TGLImage.Create(ApplicationData('walls/sandstone_wall_3.png'));
  WallSprites[4][4] := TGLImage.Create(ApplicationData('walls/sandstone_wall_4.png'));
  WallSprites[4][5] := TGLImage.Create(ApplicationData('walls/sandstone_wall_5.png'));
  WallSprites[4][6] := TGLImage.Create(ApplicationData('walls/sandstone_wall_6.png'));
  WallSprites[4][7] := TGLImage.Create(ApplicationData('walls/sandstone_wall_7.png'));
  WallSprites[4][8] := TGLImage.Create(ApplicationData('walls/sandstone_wall_8.png'));
  WallSprites[4][9] := TGLImage.Create(ApplicationData('walls/sandstone_wall_9.png'));


  SetLength(FloorSprites[0], 4);
  FloorSprites[0][0] := TGLImage.Create(ApplicationData('floors/floor_sand_rock_0.png'));
  FloorSprites[0][1] := TGLImage.Create(ApplicationData('floors/floor_sand_rock_1.png'));
  FloorSprites[0][2] := TGLImage.Create(ApplicationData('floors/floor_sand_rock_2.png'));
  FloorSprites[0][3] := TGLImage.Create(ApplicationData('floors/floor_sand_rock_3.png'));
  SetLength(FloorSprites[1], 8);
  FloorSprites[1][0] := TGLImage.Create(ApplicationData('floors/floor_sand_stone_0.png'));
  FloorSprites[1][1] := TGLImage.Create(ApplicationData('floors/floor_sand_stone_1.png'));
  FloorSprites[1][2] := TGLImage.Create(ApplicationData('floors/floor_sand_stone_2.png'));
  FloorSprites[1][3] := TGLImage.Create(ApplicationData('floors/floor_sand_stone_3.png'));
  FloorSprites[1][4] := TGLImage.Create(ApplicationData('floors/floor_sand_stone_4.png'));
  FloorSprites[1][5] := TGLImage.Create(ApplicationData('floors/floor_sand_stone_5.png'));
  FloorSprites[1][6] := TGLImage.Create(ApplicationData('floors/floor_sand_stone_6.png'));
  FloorSprites[1][7] := TGLImage.Create(ApplicationData('floors/floor_sand_stone_7.png'));
  SetLength(FloorSprites[2], 8);
  FloorSprites[2][0] := TGLImage.Create(ApplicationData('floors/sand_1.png'));
  FloorSprites[2][1] := TGLImage.Create(ApplicationData('floors/sand_2.png'));
  FloorSprites[2][2] := TGLImage.Create(ApplicationData('floors/sand_3.png'));
  FloorSprites[2][3] := TGLImage.Create(ApplicationData('floors/sand_4.png'));
  FloorSprites[2][4] := TGLImage.Create(ApplicationData('floors/sand_5.png'));
  FloorSprites[2][5] := TGLImage.Create(ApplicationData('floors/sand_6.png'));
  FloorSprites[2][6] := TGLImage.Create(ApplicationData('floors/sand_7.png'));
  FloorSprites[2][7] := TGLImage.Create(ApplicationData('floors/sand_8.png'));
end;

procedure FreeSprites;
var
  i, j: Integer;
begin
  Log(CurrentRoutine, 'Freeing sprites');
  for i := 0 to High(WallSprites) do
    for j := 0 to High(WallSprites[i]) do
      WallSprites[i][j].Free;
  for i := 0 to High(FloorSprites) do
    for j := 0 to High(FloorSprites[i]) do
      FloorSprites[i][j].Free;
end;

function RandomFloorBiome: Integer;
begin
  Result := RND.Random(High(FloorSprites) + 1);
end;
function RandomWallBiome: Integer;
begin
  Result := RND.Random(High(WallSprites) + 1);
end;


end.

