{ Copyright (C) 2018 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ --------------------------------------------------------------------------- }

(* {} *)

unit MapTools;

{$INCLUDE compilerconfig.inc}

interface

uses
  Classes, SysUtils;

type
  TMapType = array of array of Integer;

function Sign(const a: Integer): Integer;

function FillMap(const SizeX, SizeY: Integer; const aValue: Integer): TMapType;
function MapCopy(const aMap: TMapType): TMapType;

function InvertX(const aMap: TMapType): TMapType;
function InvertY(const aMap: TMapType): TMapType;
{function Rotate90(const aMap: TMapType): TMapType;
function Rotate270(const aMap: TMapType): TMapType;}

procedure MapLine(var aMap: TMapType; const x1,y1,x2,y2: Integer; const aValue: Integer);
procedure MapCircle(var aMap: TMapType; const x, y: Integer; const R: Single; const aValue: Integer);
procedure MapBox(var aMap: TMapType; const x1,y1,x2,y2: Integer; const aValue: Integer);
procedure MapRectangle(var aMap: TMapType; const x1,y1,x2,y2: Integer; const aValue: Integer);
procedure MakeBorder(var aMap: TMapType; const aValue: Integer);

{finds isolated passages and removes them}
procedure RemoveSingleNegativeUnits(var aMap: TMapType);
function CalculateNegativeUnits(const aMap: TMapType): Integer;

function Width(const aMap: TMapType): Integer;
function Height(const aMap: TMapType): Integer;
implementation
uses
  DecoLog;

function Sign(const a: Integer): Integer;
begin
  if a > 0 then
    Result := 1
  else
  if a < 0 then
    Result := -1
  else
    Result := 0;
end;

function FillMap(const SizeX, SizeY: Integer; const aValue: Integer): TMapType;
var
  ix, iy: Integer;
begin
  SetLength(Result, SizeX);
  for ix := 0 to Pred(SizeX) do
  begin
    SetLength(Result[ix], SizeY);
    for iy := 0 to Pred(SizeY) do
      Result[ix, iy] := aValue;
  end;
end;

function MapCopy(const aMap: TMapType): TMapType;
var
  ix, iy: Integer;
begin
  SetLength(Result, High(aMap) + 1);
  for ix := 0 to High(aMap) do
  begin
    SetLength(Result[ix], High(aMap[ix]) + 1);
    for iy := 0 to High(aMap[ix]) do
      Result[ix, iy] := aMap[ix, iy];
  end;
end;

function InvertX(const aMap: TMapType): TMapType;
var
  ix, iy: Integer;
  sx, sy: Integer;
begin
  sx := High(aMap);
  SetLength(Result, sx + 1);
  for ix := 0 to sx do
  begin
    sy := High(aMap[ix]);
    SetLength(Result[ix], sy + 1);
    for iy := 0 to Pred(sy) do
      Result[ix, iy] := aMap[sx - ix, iy];
  end;
end;
function InvertY(const aMap: TMapType): TMapType;
var
  ix, iy: Integer;
  sx, sy: Integer;
begin
  sx := High(aMap);
  SetLength(Result, sx + 1);
  for ix := 0 to sx do
  begin
    sy := High(aMap[ix]);
    SetLength(Result[ix], sy + 1);
    for iy := 0 to Pred(sy) do
      Result[ix, iy] := aMap[ix, sy - iy];
  end;
end;
//these will invert x-y size
{function Rotate90(const aMap: TMapType): TMapType;
function Rotate270(const aMap: TMapType): TMapType;}

procedure MapLine(var aMap: TMapType; const x1,y1,x2,y2: Integer; const aValue: Integer);
var
  w, h: Integer;
  dx, dy: Integer;
  xs, ys, xe, ye: Integer;
  ix, iy: Integer;
begin
  w := Width(aMap);
  h := Height(aMap);
  dx := Abs(x2 - x1);
  dy := Abs(y2 - y1);

  if dx > dy then
  begin
    if x2 > x1 then
    begin
      xe := x2;
      xs := x1;
      ye := y2;
      ys := y1;
    end else
    begin
      xe := x1;
      xs := x2;
      ye := y1;
      ys := y2;
    end;

    for ix := xs to xe do
    begin
      iy := Trunc(ys + (ye - ys) * (ix - xs) / (xe - xs));
      if (ix > 0) and (ix < Pred(w)) and (iy > 0) and (iy < Pred(h)) then
        aMap[ix, iy] := aValue;
      {iy := Trunc(ys + (ye - ys) * (ix - xs) / (xe - xs)) + 1;
      if (ix > 0) and (ix < Pred(w)) and (iy > 0) and (iy < Pred(h)) then
        aMap[ix, iy] := aValue;}
    end;
  end else
  begin
    if y2 > y1 then
    begin
      ye := y2;
      ys := y1;
      xe := x2;
      xs := x1;
    end else
    begin
      ye := y1;
      ys := y2;
      xe := x1;
      xs := x2;
    end;
    for iy := ys to ye do
    begin
      ix := Trunc(xs + (xe - xs) * (iy - ys) / (ye - ys));
      if (ix > 0) and (ix < Pred(w)) and (iy > 0) and (iy < Pred(h)) then
        aMap[ix, iy] := aValue;
      {ix := Trunc(xs + (xe - xs) * (iy - ys) / (ye - ys)) + 1;
      if (ix > 0) and (ix < Pred(w)) and (iy > 0) and (iy < Pred(h)) then
        aMap[ix, iy] := aValue;}
    end;
  end;
end;

procedure MapCircle(var aMap: TMapType; const x, y: Integer; const R: Single; const aValue: Integer);
var
  w, h: Integer;
  ix, iy: Integer;
begin
  w := Width(aMap);
  h := Height(aMap);
  for ix := -Trunc(R)-1 to Trunc(R)+1 do
    for iy := -Trunc(R)-1 to Trunc(R)+1 do
      if (x+ix > 0) and (x+ix < Pred(w)) and (y+iy > 0) and (y+iy < Pred(h)) and
        (Sqr(ix) + Sqr(iy) <= Sqr(R)) then
           aMap[x+ix, y+iy] := aValue;
end;

procedure MapBox(var aMap: TMapType; const x1,y1,x2,y2: Integer; const aValue: Integer);
var
  xs, ys, xe, ye: Integer;
  ix, iy: Integer;
begin
  if x1 > x2 then
  begin
    xs := x2;
    xe := x1;
  end else
  begin
    xs := x1;
    xe := x2;
  end;
  if y1 > y2 then
  begin
    ys := y2;
    ye := y1;
  end else
  begin
    ys := y1;
    ye := y2;
  end;
  for ix := xs to xe do
  begin
    aMap[ix, ys] := aValue;
    aMap[ix, ye] := aValue;
  end;
  for iy := ys to ye do
  begin
    aMap[xs, iy] := aValue;
    aMap[xe, iy] := aValue;
  end;
end;

procedure MapRectangle(var aMap: TMapType; const x1,y1,x2,y2: Integer; const aValue: Integer);
var
  w, h: Integer;
  xs, ys, xe, ye: Integer;
  ix, iy: Integer;
begin
  w := Width(aMap);
  h := Height(aMap);
  if x1 > x2 then
  begin
    xs := x2;
    xe := x1;
  end else
  begin
    xs := x1;
    xe := x2;
  end;
  if y1 > y2 then
  begin
    ys := y2;
    ye := y1;
  end else
  begin
    ys := y1;
    ye := y2;
  end;
  for ix := xs to xe do
    for iy := ys to ye do
      if (ix>0) and (ix < Pred(w)) and (iy > 0) and (iy < Pred(h)) then
        aMap[ix, iy] := aValue;
end;

procedure MakeBorder(var aMap: TMapType; const aValue: Integer);
var
  w, h: Integer;
  ix, iy: Integer;
begin
  w := Width(aMap);
  h := Height(aMap);
  for ix := 0 to Pred(w) do
  begin
    aMap[ix, 0] := aValue;
    aMap[ix, Pred(h)] := aValue;
  end;
  for iy := 0 to Pred(h) do
  begin
    aMap[0, iy] := aValue;
    aMap[Pred(w), iy] := aValue;
  end;

end;

procedure RemoveSingleNegativeUnits(var aMap: TMapType);
var
  ix, iy, dx, dy: Integer;
  flg: Boolean;
begin
  for ix := 1 to High(aMap)-1 do
    for iy := 1 to High(aMap[ix])-1 do
    if aMap[ix, iy] = -1 then
    begin
      flg := true;
      for dx := -1 to 1 do
        for dy := -1 to 1 do
          if (dx<>0) or (dy<>0) then
            if aMap[ix+dx, iy+dy] = -1 then
              flg := false;
      if flg then
        aMap[ix, iy] := aMap[ix+1, iy];
    end;
end;

function Width(const aMap: TMapType): Integer;
begin
  Result := High(aMap) + 1;
end;
function Height(const aMap: TMapType): Integer;
begin
  Result := High(aMap[0]) + 1;
end;

function CalculateNegativeUnits(const aMap: TMapType): Integer;
var
  ix, iy: Integer;
begin
  Result := 0;
  for ix := 1 to High(aMap)-1 do
    for iy := 1 to High(aMap[ix])-1 do
      if aMap[ix, iy] = -1 then
        inc(Result);
end;

end.


