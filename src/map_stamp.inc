//Parameter1 is the stamp used
procedure TMap.MapStamp(out Floor, Wall: TMapType; StampBlock: TMapType);
var
  ix, iy, dx, dy, sx, sy, w, h: Integer;
  BiomeFloor, BiomeWall: Integer;
begin
  Log(CurrentRoutine, 'Generating map...');
  BiomeFloor := RandomFloorBiome;
  BiomeWall := RandomWallBiome;
  Floor := FillMap(SizeX, SizeY, BiomeFloor);
  Wall := FillMap(SizeX, SizeY, BiomeWall);

  w := Width(StampBlock);
  h := Height(StampBlock);
  //start phase offset
  sx := - RND.Random(w);
  sy := - RND.Random(h);
  for ix := 0 to SizeX div w + 3 do
    for iy := 0 to SizeY div h + 3 do
      for dx := 0 to Pred(w) do
        for dy := 0 to Pred(h) do
          if Safe(ix*w+dx+sx, iy*h+dy+sy) then
          begin
            if RND.Random < StampBlock[dx, dy]/8 then
              Wall[ix*w+dx+sx, iy*h+dy+sy] := BiomeWall
            else
              Wall[ix*w+dx+sx, iy*h+dy+sy] := -1
          end;
end;
