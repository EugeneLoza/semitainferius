//No parameters
procedure TMap.MapEgg(out Floor, Wall: TMapType);
var
  BiomeFloor, BiomeWall: Integer;
  cx, cy: Integer;
  R: Integer;
  v: Integer;
  i: Integer;
  phi: Single;
begin
  Log(CurrentRoutine, 'Generating map...');
  BiomeFloor := RandomFloorBiome;
  BiomeWall := RandomWallBiome;

  cx := SizeX div 5 + RND.Random(SizeX - SizeX div 5);
  cy := SizeY div 5 + RND.Random(SizeY - SizeY div 5);

  Floor := FillMap(SizeX, SizeY, BiomeFloor);
  Wall := FillMap(SizeX, SizeY, BiomeWall);
  R := SizeX + SizeY;
  v := -1;
  repeat
    dec(R, 1 + RND.Random(3));
    if v = -1 then
      v := BiomeWall
    else
      v := -1;
    MapCircle(Wall, cx, cy, R, v);
    if v = BiomeWall then
      for i := 0 to 3 + RND.Random(Round(R)) do
      begin
        phi := (RND.Random * 2 * Pi);
        MapLine(Wall, cx, cy, Round(cx + (R+1) * Cos(phi)), Round(cy + (R+1) * Sin(Phi)), -1)
      end;
  until R < 4;
end;

