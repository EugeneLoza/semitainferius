//Parameter1 is seed
procedure TMap.MapRandomBlocks(out Floor, Wall: TMapType; Parameter1: Single);
var
  ix, iy, jx, jy: Integer;
  BiomeFloor, BiomeWall: Integer;
  Seed: Single;
  w, h: Integer;
  sx, sy: Integer;
  v: Integer;
begin
  Log(CurrentRoutine, 'Generating map...');
  BiomeFloor := RandomFloorBiome;
  BiomeWall := RandomWallBiome;

  Seed := 0.3 + Sqr(Parameter1) * 0.5;

  w := 2 + RND.Random(3);
  h := 2 + RND.Random(3);

  sx := -RND.Random(w);
  sy := -RND.Random(h);

  Floor := FillMap(SizeX, SizeY, BiomeFloor);
  Wall := FillMap(SizeX, SizeY, BiomeWall);
  for ix := 0 to Pred(SizeX) div w + 3 do
    for iy := 0 to Pred(SizeY) div h + 3 do
    begin
      if RND.Random < Seed then
        v := -1
      else
        v := BiomeWall;
      for jx := ix*w+sx to (ix+1)*w+sx-1 do
        for jy := iy*h+sy to (iy+1)*h+sy-1 do
          if (jx > 0) and (jx < Pred(SizeX)) and (jy > 0) and (jy < Pred(SizeY)) then
          begin
            Floor[jx, jy] := BiomeFloor;
            Wall[jx, jy] := v;
          end;
    end;
end;

