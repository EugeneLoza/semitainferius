{ Copyright (C) 2018 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ --------------------------------------------------------------------------- }

(* {} *)

unit MapUnit;

{$INCLUDE compilerconfig.inc}

interface

uses
  CastleVectors, CastleGLImages,
  MapTools;

type
  {}
  TMap = class(TObject)
  strict protected
    procedure MapRandom(out Floor, Wall: TMapType; Parameter1: Single);
    procedure MapRandomBlocks(out Floor, Wall: TMapType; Parameter1: Single);
    procedure MapCircles(out Floor, Wall: TMapType);
    procedure MapEgg(out Floor, Wall: TMapType);
    procedure MapEggre(out Floor, Wall: TMapType);
    procedure MapConcentric(out Floor, Wall: TMapType);
    procedure MapBoxes(out Floor, Wall: TMapType);
    procedure MapRectagonal(out Floor, Wall: TMapType);
    procedure MapRotor(out Floor, Wall: TMapType);
    procedure MapTMap(out Floor, Wall: TMapType; Parameter1: Single);
    procedure MapAntiCircles(out Floor, Wall: TMapType);
    procedure MapCocon(out Floor, Wall: TMapType; Parameter1: Single; Parameter2, Parameter3: Boolean);
    procedure MapStamp(out Floor, Wall: TMapType; StampBlock: TMapType);
  strict protected
    procedure PlaceBlock(const Block: TMapType; const ax, ay: Integer);
    procedure MixMap(out Floor, Wall: TMapType);
    procedure Flood;
    function MapGood: Boolean;
  strict protected
    WallShade, FloorShade: TVector4;
    BaseHash: String;
    function FloorHash(const ax, ay: Integer): Integer;
    function WallHash(const ax, ay: Integer): Integer;
    function Safe(const ax, ay: Integer): Boolean;
    function SafeWall(const ax, ay: Integer): Boolean;
    function SafePass(const ax, ay: Integer): Boolean;
    function MapArea: Integer;
  public
    SizeX, SizeY: Integer;
    ScaleX, ScaleY: Single;
    WallMap, FloorMap, FloorVariant, VisibleMap: TMapType;
    StartX, StartY, EndX, EndY: Integer;

    procedure Draw(const DisplayX, DisplayY: Integer);
    procedure GenerateMap(const aSizeX, aSizeY: Integer);
    procedure ClearVisible;
  public
    constructor Create;
    destructor Destroy; override;
  end;

var
  Map: TMap;

implementation
uses
  SysUtils,
  CastleRandom,
  Global, SpritesUnit, MapBlocks,
  DecoLog;

function TMap.FloorHash(const ax, ay: Integer): Integer;
begin
  Result := Trunc( (High(FloorSprites[FloorMap[ax, ay]]) + 1) *
    StringToHash(BaseHash + IntToStr(ax) + 'x' + IntToStr(ay)) / High(LongWord));
end;
function TMap.WallHash(const ax, ay: Integer): Integer;
begin
  Result := Trunc( (High(WallSprites[WallMap[ax, ay]]) + 1) *
    StringToHash(BaseHash + IntToStr(ax) + 'x' + IntToStr(ay)) / High(LongWord));
end;
function TMap.Safe(const ax, ay: Integer): Boolean;
begin
  Result := (ax > 0) and (ax < Pred(SizeX)) and (ay > 0) and (ay < Pred(SizeY));
end;
function TMap.SafeWall(const ax, ay: Integer): Boolean;
begin
  if Safe(ax, ay) then
    Result := WallMap[ax, ay] >= 0
  else
    Result := true
end;
function TMap.SafePass(const ax, ay: Integer): Boolean;
begin
  if Safe(ax, ay) then
    Result := WallMap[ax, ay] = -1
  else
    Result := false
end;
function TMap.MapArea: Integer;
begin
  Result := SizeX * SizeY;
end;

constructor TMap.Create;
begin
end;
destructor TMap.Destroy;
begin
  inherited Destroy;
end;

procedure TMap.ClearVisible;
begin
  VisibleMap := FillMap(SizeX, SizeY, -1);
end;

procedure TMap.Draw(const DisplayX, DisplayY: Integer);
var
  ix, iy: Integer;
  x1, y1, x2, y2, w, h: Single;
begin
  for ix := 0 to Pred(SizeX) do
    for iy := 0 to Pred(SizeY) do
      if true{onscreen} then
      begin
        x1 := (ix - DisplayX) * ScaleX;
        y1 := (iy - DisplayY) * ScaleY;
        x2 := (ix - DisplayX + 1) * ScaleX;
        y2 := (iy - DisplayY + 1) * ScaleY;
        w := x2 - x1;
        h := y2 - y1;
        if FloorMap[ix, iy] >= 0 then
          with FloorSprites[FloorMap[ix, iy]][FloorHash(ix, iy)] do
          begin
            Color := FloorShade;
            Draw(x1, y1, w, h);
          end;
        if WallMap[ix, iy] >= 0 then
          with WallSprites[WallMap[ix, iy]][WallHash(ix, iy)] do
          begin
            Color := WallShade;
            Draw(x1, y1, w, h);
          end;
      end;
end;

procedure TMap.PlaceBlock(const Block: TMapType; const ax, ay: Integer);
var
  ix, iy: Integer;
  BiomeFloor, BiomeWall: Integer;
begin
  BiomeFloor := RandomFloorBiome;
  BiomeWall := RandomWallBiome;

  for iy := 0 to High(Block) do
    for ix := 0 to High(Block[iy]) do
      if Safe(ix + ax, iy + ay) then
      begin
        FloorMap[ix + ax, iy + ay] := BiomeFloor;
        if Block[iy, ix] = 9 then
          //don't change the map
        else
        if (Rnd.Random <= Block[iy, ix] / 8) then
          WallMap[ix + ax, iy + ay] := BiomeWall
        else
          WallMap[ix + ax, iy + ay] := -1;
      end;
end;

procedure TMap.Flood;
var
  ix, iy: Integer;
  dx, dy: Integer;
  Count: Integer;
  CurrentPass: Integer;
  FloodMap, FloodMapOld: TMapType;
  d, r: Integer;
begin
  FloodMap := FillMap(SizeX, SizeY, -1);
  FloodMap[StartX, StartY] := 0;
  CurrentPass := 0;
  repeat
    Count := 0;
    inc(CurrentPass);
    FloodMapOld := MapCopy(FloodMap);

    for ix := 1 to Pred(SizeX) - 1 do
      for iy := 1 to Pred(SizeY) - 1 do
        if (FloodMapOld[ix, iy] = -1) and (SafePass(ix, iy)) then
          for dx := -1 to 1 do
            for dy := -1 to 1 do
              if (dx <> 0) or (dy<>0) then
                if (SafePass(ix+dx, iy+dy)) and (FloodMapOld[ix+dx, iy+dy] >= 0) then
                begin
                  inc(Count);
                  FloodMap[ix, iy] := CurrentPass;
                end;
  until Count = 0;
  FloodMapOld := nil;

  repeat
    Count := 0;
    for ix := 1 to Pred(SizeX) - 1 do
      for iy := 1 to Pred(SizeY) - 1 do
        if (FloodMap[ix, iy] = -1) and (SafePass(ix, iy)) then
          for dx := -1 to 1 do
            for dy := -1 to 1 do
              if (dx <> 0) or (dy<>0) then
                if (SafeWall(ix+dx, iy+dy)) then
                begin
                  inc(Count);
                  WallMap[ix, iy] := WallMap[ix+dx, iy+dy];
                end;
  until Count = 0;

  d := 0;
  for ix := 1 to Pred(SizeX) - 1 do
    for iy := 1 to Pred(SizeY) - 1 do
      if (FloodMap[ix, iy] > CurrentPass - 7) then
      begin
        r := Sqr(ix - StartX) + Sqr(iy - StartY);
        if r > d then
        begin
          EndX := ix;
          EndY := iy;
          d := r;
        end;
      end;
  Log(CurrentRoutine, 'Exit at ' + IntToStr(EndX) + ',' + IntToStr(EndY));
  if EndX < 2 then
    EndX := 2;
  if EndY < 2 then
    EndY := 2;
  if EndY > SizeY - 3 then
    EndY := SizeY - 3;
  if EndX > SizeX - 3 then
    EndX := SizeX - 3;
end;

function TMap.MapGood: Boolean;
const
  FracX = 5;
  FracY = 5;
var
  ix, iy, jx, jy: Integer;
  F: array[0..Pred(FracX), 0..Pred(FracY)] of Integer;
  Count: Integer;

  FreeArea: Integer;
  EmptyFrac: Integer;
  AverageFrac, Deviation: Single;
begin
  FreeArea := 0;
  for ix := 0 to Pred(FracX) do
    for iy := 0 to Pred(FracY) do
    begin
      Count := 0;
      for jx := ix * SizeX div FracX to Pred((ix + 1) * SizeX div FracX) do
        for jy := iy * SizeY div FracY to Pred((iy + 1) * SizeY div FracY) do
          if SafePass(jx, jy) then
            inc(Count);
      F[ix, iy] := Count;
      Inc(FreeArea, Count);
    end;
  Log(CurrentRoutine, 'FreeArea = ' + FloatToStr(100 * FreeArea / MapArea)+ '%');

  if FreeArea/MapArea < 0.2 then
    Exit(false);
  if FreeArea/MapArea > 0.7 then
    Exit(false);

  EmptyFrac := 0;
  for ix := 0 to Pred(FracX) do
    for iy := 0 to Pred(FracY) do
      if F[ix, iy] = 0 then
        inc(EmptyFrac);
  Log(CurrentRoutine, 'EmptyFrac = ' + IntToStr(EmptyFrac));
  if EmptyFrac > 2 then
    Exit(false);

  AverageFrac := 0;
  for ix := 0 to Pred(FracX) do
    for iy := 0 to Pred(FracY) do
      AverageFrac += F[ix, iy];
  AverageFrac := AverageFrac / FracX / FracY;
  Log(CurrentRoutine, 'AverageFrac = ' + FloatToStr(AverageFrac));
  Deviation := 0;
  for ix := 0 to Pred(FracX) do
    for iy := 0 to Pred(FracY) do
      Deviation += Sqr(F[ix, iy] - AverageFrac);
  Deviation := Sqrt(Deviation) {/ FracX / FracY};
  Log(CurrentRoutine, 'Deviation/Area = ' + FloatToStr(Deviation / MapArea {* FracX * FracY}));

  Result := true;
end;

procedure TMap.GenerateMap(const aSizeX, aSizeY: Integer);
begin
  WallShade := Vector4(0.7 + RND.Random * 0.5, 0.7 + RND.Random * 0.5, 0.7 + RND.Random * 0.5, 1);
  FloorShade := Vector4(0.7 + RND.Random * 0.5, 0.7 + RND.Random * 0.5, 0.7 + RND.Random * 0.5, 1);

  SizeX := aSizeX;
  SizeY := aSizeY;
  ScaleX := 600 div SizeX;
  ScaleY := 600 div SizeX;

  {if RND.RandomBoolean then
    StartX := 2
  else
    StartX := SizeX - 3;
  if RND.RandomBoolean then
    StartY := 2
  else
    StartY := SizeY - 3;}
  StartX := 2;
  StartY := SizeY - 3;

  BaseHash := IntToStr(Rnd.Random32bit);

  repeat
    MixMap(WallMap, FloorMap);
    PlaceBlock(BBunker, StartX - 3, StartY - 3); //entrance bunker
    Flood;
    PlaceBlock(BExit, EndX - 1, EndY - 1); //exit bunker
  until MapGood;

  ClearVisible;
end;

procedure TMap.MixMap(out Floor, Wall: TMapType);
var
  BiomeMap: TMapType;
  BiomeWallMap, BiomeFloorMap: array of TMapType;
  NBiomes: Integer = 1;
  i: Integer;
  ix, iy: Integer;
begin
  BiomeMap := FillMap(SizeX, SizeY, 0); //only one biome now

  SetLength(BiomeWallMap, NBiomes);
  SetLength(BiomeFloorMap, NBiomes);
  for i := 0 to Pred(NBiomes) do
    //MapStamp(BiomeWallMap[i], BiomeFloorMap[i], SDiamond5);
    {case RND.Random(9) of
       0: MapRandom(BiomeWallMap[i], BiomeFloorMap[i], RND.Random);
       1: MapRandomBlocks(BiomeWallMap[i], BiomeFloorMap[i], RND.Random);
       2: MapCocon(BiomeWallMap[i], BiomeFloorMap[i], RND.Random, RND.RandomBoolean, RND.RandomBoolean);
       3: MapStamp(BiomeWallMap[i], BiomeFloorMap[i], SCheckers);
       4: MapStamp(BiomeWallMap[i], BiomeFloorMap[i], SWhirl4);
       5: MapStamp(BiomeWallMap[i], BiomeFloorMap[i], SDiamond3);
       6: MapStamp(BiomeWallMap[i], BiomeFloorMap[i], SDiamond5);
       7: MapStamp(BiomeWallMap[i], BiomeFloorMap[i], SDiamond7);
       9: MapCircles(BiomeWallMap[i], BiomeFloorMap[i]);
      10: MapAntiCircles(BiomeWallMap[i], BiomeFloorMap[i]);
      11: MapTMap(BiomeWallMap[i], BiomeFloorMap[i]);
      12: MapRectagonal(BiomeWallMap[i], BiomeFloorMap[i]);
      13: MapBoxes(BiomeWallMap[i], BiomeFloorMap[i]);
      14: MapConcentric(BiomeWallMap[i], BiomeFloorMap[i]);
      15: MapEgg(BiomeWallMap[i], BiomeFloorMap[i]);
      16: MapEggre(BiomeWallMap[i], BiomeFloorMap[i]);
      17: MapRotor(BiomeWallMap[i], BiomeFloorMap[i]);
    end;}
    MapRotor(BiomeWallMap[i], BiomeFloorMap[i]);

  //now mix the map into biome

  Wall := FillMap(SizeX, SizeY, -1);
  Floor := FillMap(SizeX, SizeY, -1);
  for ix := 0 to Pred(SizeX) do
    for iy := 0 to Pred(SizeY) do
    begin
      Wall[ix, iy] := BiomeWallMap[BiomeMap[ix, iy]][ix, iy];
      Floor[ix, iy] := BiomeFloorMap[BiomeMap[ix, iy]][ix, iy];
    end;
end;


{$INCLUDE map_random.inc}
{$INCLUDE map_cocon.inc}
{$INCLUDE map_stamp.inc}
{$INCLUDE map_randomblocks.inc}
{$INCLUDE map_circles.inc}
{$INCLUDE map_anticircles.inc}
{$INCLUDE map_tmap.inc}
{$INCLUDE map_rectagonal.inc}
{$INCLUDE map_boxes.inc}
{$INCLUDE map_concentric.inc}
{$INCLUDE map_egg.inc}
{$INCLUDE map_eggre.inc}
{$INCLUDE map_rotor.inc}

end.

