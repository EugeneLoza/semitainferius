{ Copyright (C) 2018 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ --------------------------------------------------------------------------- }

(* {} *)

unit MainUnit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

{ Assign Window.onUpdate event }
procedure InitMain;
{ Release Window.onUpdate event }
procedure FreeMain;

implementation
uses
  CastleWindow, CastleKeysMouse,
  MapUnit,
  WindowUnit, DecoLog, DecoTime;

{$PUSH}{$WARN 5024 off : Parameter "$1" not used} // It's ok to have unused variables here - we don't need a Container at the moment

procedure doPress(Container: TUIContainer; const Event: TInputPressRelease);
begin
  if Event.EventType = itKey then
    if Event.Key = keyF5 then
      Map.GenerateMap(64, 64);
end;

{--------------------------------------------------------------------------}

procedure doRelease(Container: TUIContainer; const Event: TInputPressRelease);
begin

end;

{--------------------------------------------------------------------------}

procedure doMotion(Container: TUIContainer; const Event: TInputMotion);
begin

end;

{............................................................................}
procedure doUpdate(Container: TUIContainer);
begin

end;

{-----------------------------------------------------------------------------}

procedure doRender(Container: TUIContainer);
begin
  doTime;
  //no clearing screen
  Map.Draw(0,0);
end;

{$POP}

{============================================================================}

procedure InitMain;
begin
  Window.OnUpdate := @doUpdate;
  Window.OnRender := @doRender;
  Window.OnPress := @doPress;
  Window.OnRelease := @doRelease;
  Window.OnMotion := @doMotion;
end;

{-----------------------------------------------------------------------------}

procedure FreeMain;
begin
  Window.OnUpdate := nil;
  Window.OnRender := nil;
  Window.OnPress := nil;
  Window.OnRelease := nil;
  Window.OnMotion := nil;
end;

end.

