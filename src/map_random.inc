//Parameter1 is seed
procedure TMap.MapRandom(out Floor, Wall: TMapType; Parameter1: Single);
var
  ix, iy: Integer;
  BiomeFloor, BiomeWall: Integer;
  Seed: Single;
begin
  Log(CurrentRoutine, 'Generating map...');
  BiomeFloor := RandomFloorBiome;
  BiomeWall := RandomWallBiome;

  Seed := 0.3 + Sqr(Parameter1) * 0.5;

  Floor := FillMap(SizeX, SizeY, BiomeFloor);
  Wall := FillMap(SizeX, SizeY, BiomeWall);
  for ix := 1 to Pred(SizeX)-1 do
    for iy := 1 to Pred(SizeY)-1 do
    begin
      Floor[ix, iy] := BiomeFloor;
      if RND.Random < Seed then
        Wall[ix, iy] := -1
      else
        Wall[ix, iy] := BiomeWall;
    end;
end;

