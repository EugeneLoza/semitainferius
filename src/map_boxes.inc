//No parameters
procedure TMap.MapBoxes(out Floor, Wall: TMapType);
var
  x1, y1, x2, y2: Integer;
  BiomeFloor, BiomeWall: Integer;
  MinLength, MaxLength: Integer;
begin
  Log(CurrentRoutine, 'Generating map...');
  BiomeFloor := RandomFloorBiome;
  BiomeWall := RandomWallBiome;

  MinLength := 3;
  MaxLength := (SizeX + SizeY) div 8;

  Floor := FillMap(SizeX, SizeY, BiomeFloor);
  Wall := FillMap(SizeX, SizeY, BiomeWall);
  repeat
    x1 := RND.Random(SizeX - 2) + 1;
    y1 := RND.Random(SizeY - 2) + 1;
    x2 := RND.Random(SizeX - 2) + 1;
    y2 := RND.Random(SizeY - 2) + 1;
    if (abs(x1 - x2) >= MinLength) and (abs(x1 - x2) <= MaxLength) and
       (abs(y1 - y2) >= MinLength) and (abs(y1 - y2) <= MaxLength) then
          MapBox(Wall, x1, y1, x2, y2, -1);
  until CalculateNegativeUnits(Wall) > MapArea * 0.3;
end;

