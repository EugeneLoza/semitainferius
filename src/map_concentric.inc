//Parameter1 is seed
procedure TMap.MapConcentric(out Floor, Wall: TMapType);
var
  BiomeFloor, BiomeWall, BiomeWall2: Integer;
  minR1, maxR1, minR2, maxR2, R1, R2: Single;
  w: Integer;
  ix, iy, sx, sy: Integer;
begin
  Log(CurrentRoutine, 'Generating map...');
  BiomeFloor := RandomFloorBiome;
  BiomeWall := RandomWallBiome;
  BiomeWall2 := RandomWallBiome;

  w := 7 + RND.Random(5);

  minR1 := 1;
  maxR1 := w/3;
  minR2 := 2;
  maxR2 := w/2;

  sx := -RND.Random(w);
  sy := -RND.Random(w);

  Floor := FillMap(SizeX, SizeY, BiomeFloor);
  Wall := FillMap(SizeX, SizeY, BiomeWall);
  for ix := 0 to Pred(SizeX) div w + 3 do
    for iy := 0 to Pred(SizeY) div w + 3 do
    begin
      R2 := minR2 + RND.Random * (maxR2 - minR2 + 1);
      R1 := minR1 + RND.Random * (maxR1 - minR1 + 1);
      if R1 > R2 - 1 then
        R1 := R2 - 1;
      MapCircle(Wall, ix*w+sx, iy*w+sy, R2, -1);
      MapCircle(Wall, ix*w+sx, iy*w+sy, R1, BiomeWall2);
    end;
end;

