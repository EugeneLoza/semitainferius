{ Copyright (C) 2018 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ --------------------------------------------------------------------------- }

(* {} *)

unit Game;

{$INCLUDE compilerconfig.inc}

interface

uses
  Classes, SysUtils;

procedure NewGame;
procedure EndGame;
implementation
uses
  MapUnit;

procedure NewGame;
begin
  FreeAndNil(Map);
  Map := TMap.Create;
  Map.GenerateMap(64, 64);
end;

procedure EndGame;
begin
  FreeAndNil(Map);
end;

end.

