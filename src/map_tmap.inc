//Parameter1 is seed
procedure TMap.MapTMap(out Floor, Wall: TMapType; Parameter1: Single);
var
  ix, iy, dx, dy, sx, sy, w, h: Integer;
  BiomeFloor, BiomeWall: Integer;
  Seed: Single;
begin
  Log(CurrentRoutine, 'Generating map...');
  BiomeFloor := RandomFloorBiome;
  BiomeWall := RandomWallBiome;
  Floor := FillMap(SizeX, SizeY, BiomeFloor);
  Wall := FillMap(SizeX, SizeY, BiomeWall);
  Seed := 0.3 + Parameter1 * 0.3;

  w := Width(FTMap);
  h := Height(FTMap);
  //start phase offset
  sx := - RND.Random(w);
  sy := - RND.Random(h);
  for ix := 0 to SizeX div w + 3 do
    for iy := 0 to SizeY div h + 3 do
    begin
      for dx := 0 to Pred(w) do
        for dy := 0 to Pred(h) do
          if Safe(ix*w+dx+sx, iy*h+dy+sy) then
          begin
            if RND.Random < FTMap[dx, dy]/8 then
              Wall[ix*w+dx+sx, iy*h+dy+sy] := BiomeWall
            else
              Wall[ix*w+dx+sx, iy*h+dy+sy] := -1
          end;
      if RND.Random < Seed then
      begin
        dx := 1;
        dy := 0;
        if Safe(ix*w+dx+sx, iy*h+dy+sy) then
          Wall[ix*w+dx+sx, iy*h+dy+sy] := BiomeWall;
        dx := 2;
        if Safe(ix*w+dx+sx, iy*h+dy+sy) then
          Wall[ix*w+dx+sx, iy*h+dy+sy] := BiomeWall;
      end;
      if RND.Random < Seed then
      begin
        dx := 1;
        dy := Pred(h);
        if Safe(ix*w+dx+sx, iy*h+dy+sy) then
          Wall[ix*w+dx+sx, iy*h+dy+sy] := BiomeWall;
        dx := 2;
        if Safe(ix*w+dx+sx, iy*h+dy+sy) then
          Wall[ix*w+dx+sx, iy*h+dy+sy] := BiomeWall;
      end;
      if RND.Random < Seed then
      begin
        dx := 0;
        dy := 1;
        if Safe(ix*w+dx+sx, iy*h+dy+sy) then
          Wall[ix*w+dx+sx, iy*h+dy+sy] := BiomeWall;
        dy := 2;
        if Safe(ix*w+dx+sx, iy*h+dy+sy) then
          Wall[ix*w+dx+sx, iy*h+dy+sy] := BiomeWall;
      end;
      if RND.Random < Seed then
      begin
        dx := Pred(w);
        dy := 1;
        if Safe(ix*w+dx+sx, iy*h+dy+sy) then
          Wall[ix*w+dx+sx, iy*h+dy+sy] := BiomeWall;
        dy := 2;
        if Safe(ix*w+dx+sx, iy*h+dy+sy) then
          Wall[ix*w+dx+sx, iy*h+dy+sy] := BiomeWall;
      end;
    end;
end;

