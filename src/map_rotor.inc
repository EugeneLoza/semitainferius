//No parameters
procedure TMap.MapRotor(out Floor, Wall: TMapType);
var
  x1, y1: Integer;
  BiomeFloor, BiomeWall: Integer;
  MinLength, MaxLength: Integer;
  i, Angles: Integer;
  Phi: array of Single;
  L: Single;
begin
  Log(CurrentRoutine, 'Generating map...');
  BiomeFloor := RandomFloorBiome;
  BiomeWall := RandomWallBiome;

  MinLength := 3;
  MaxLength := (SizeX + SizeY) div 4;

  Angles := 3 + RND.Random(6);
  if Angles = 4 then
    Angles := 5 + RND.Random(3);
  SetLength(Phi, Angles);
  for i := 0 to Pred(Angles) do
    Phi[i] := 2 * Pi / Angles * i;

  Floor := FillMap(SizeX, SizeY, BiomeFloor);
  Wall := FillMap(SizeX, SizeY, BiomeWall);

  repeat
    x1 := RND.Random(SizeX);
    y1 := RND.Random(SizeY);
    L := MinLength + Sqr(Sqr(Sqr(RND.Random))) * MaxLength;
    i := RND.Random(Angles);
    MapLine(Wall, x1, y1, Round(x1 + L * Sin(Phi[i])), Round(y1 + L * Cos(Phi[i])), -1);
  until CalculateNegativeUnits(Wall) > MapArea * 0.3;
end;

