//Parameter1 is seed
//Parameter2 is true: Hollow cocon, false: Wall cocon
//Parameter3 is only for Parameter2 = false: makes a room inside the cocon
procedure TMap.MapCocon(out Floor, Wall: TMapType; Parameter1: Single; Parameter2, Parameter3: Boolean);
var
  ix, iy: Integer;
  BiomeFloor, BiomeWall: Integer;
  Seed: Single;
  Prob: Single;
  dx, dy: Integer;
  Count, flg: Integer;
begin
  Log(CurrentRoutine, 'Generating map...');
  BiomeFloor := RandomFloorBiome;
  BiomeWall := RandomWallBiome;

  Seed := 0.7 + 0.3 * Parameter1;
  if Parameter3 then
    Seed := Seed * 0.8;

  Floor := FillMap(SizeX, SizeY, BiomeFloor);
  Wall := FillMap(SizeX, SizeY, BiomeWall);
  for ix := 1 to Pred(SizeX)-1 do
    for iy := 1 to Pred(SizeY)-1 do
    begin
      Floor[ix, iy] := BiomeFloor;
      Prob := Seed * (Sqr(ix - SizeX / 2) + Sqr(iy - SizeY / 2)) / (Sqr(SizeX / 2) + Sqr(SizeY / 2));
      if Parameter2 then
      begin
        if RND.Random < sqrt(Prob) then
          Wall[ix, iy] := BiomeWall
        else
          Wall[ix, iy] := -1;
      end else
      begin
        if RND.Random < Sqrt(Prob) then
          Wall[ix, iy] := -1
        else
          Wall[ix, iy] := BiomeWall;
      end;
    end;

  if (not Parameter2) and (Parameter3) then
  begin
    //purge isolated cells
    RemoveSingleNegativeUnits(Wall);
    //make an internal cavity
    Wall[SizeX div 2, SizeY div 2] := -9;
    repeat
      Count := 0;
      for ix := 1 to Pred(SizeX) - 1 do
        for iy := 1 to Pred(SizeY) - 1 do
          if Wall[ix, iy] >= 0 then
          begin
            flg := 0;
            for dx := -1 to 1 do
              for dy := -1 to 1 do
                if ((dx<>0) or (dy<>0)) and (flg > -1) then
                begin
                  if Wall[ix+dx, iy+dy] = -9 then
                    flg := 1;
                  if Wall[ix+dx, iy+dy] = -1 then
                    flg := -1;
                end;

            if flg = 1 then
            begin
              Wall[ix, iy] := -9;
              inc(Count);
            end;
          end;
    until Count = 0;

    Count := 0;
    BiomeFloor := RandomFloorBiome;
    for ix := 1 to Pred(SizeX) - 1 do
      for iy := 1 to Pred(SizeY) - 1 do
        if Wall[ix, iy] = -9 then
        begin
          Wall[ix, iy] := -1;
          Floor[ix, iy] := BiomeFloor;
          inc(Count);
        end;
    Log(CurrentRoutine, 'Total tiles in cocon hollow = ' + IntToStr(Count));

    //and make a line to freedom
    if RND.RandomBoolean then
    begin
      ix := RND.Random(SizeX - 10) + 5;
      iy := RND.Random(5) + 5;
    end else
    begin
      ix := SizeX - 5 - RND.Random(5);
      iy := RND.Random(SizeY - 10) + 5;
    end;
    MapLine(Wall, SizeX div 2, SizeY div 2, ix, iy, -1);
  end;
end;
