{ Copyright (C) 2018 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ --------------------------------------------------------------------------- }

(* {} *)

unit MapBlocks;

{$INCLUDE compilerconfig.inc}

interface

uses
  MapTools;

const
  BBunker: TMapType =
    (
     (9,9,1,0,1,9,9),
     (9,8,8,0,8,8,9),
     (1,8,0,0,0,8,1),
     (0,0,0,0,0,0,0),
     (1,8,0,0,0,8,1),
     (9,8,8,0,8,8,9),
     (9,9,1,0,1,9,9)
     );

const
  BExit: TMapType =
    (
     (0,0,0),
     (0,0,0),
     (0,0,0)
     );

const
  FTMap: TMapType =
    (
     (8,0,0,8),
     (0,0,0,0),
     (0,0,0,0),
     (8,0,0,8)
    );
const
  SCheckers: TMapType =
    (
     (8,8,2,2),
     (8,8,2,2),
     (2,2,8,8),
     (2,2,8,8)
     );

const
  SWhirl4: TMapType =
    (
     (2,8,1,2),
     (1,4,4,8),
     (8,4,4,1),
     (2,1,8,2)
    );

const
  SDiamond3: TMapType =
    (
     (7,3,7),
     (3,0,3),
     (7,3,7)
    );

const
  SDiamond5: TMapType =
    (
     (8,8,2,8,8),
     (8,0,0,0,8),
     (2,0,2,0,2),
     (8,0,0,0,8),
     (8,8,2,8,8)
    );

const
  SDiamond7: TMapType =
    (
     (8,8,8,2,8,8,8),
     (8,8,0,0,0,8,8),
     (8,0,0,0,0,0,8),
     (2,0,0,3,0,0,2),
     (8,0,0,0,0,0,8),
     (8,8,0,0,0,8,8),
     (8,8,8,2,8,8,8)
    );

implementation

{

4x4

(
 (0,0,0,0),
 (0,0,0,0),
 (0,0,0,0),
 (0,0,0,0)
);

5x5

(
 (0,0,0,0,0),
 (0,0,0,0,0),
 (0,0,0,0,0),
 (0,0,0,0,0),
 (0,0,0,0,0)
);

7x7

(
 (0,0,0,0,0,0,0),
 (0,0,0,0,0,0,0),
 (0,0,0,0,0,0,0),
 (0,0,0,0,0,0,0),
 (0,0,0,0,0,0,0),
 (0,0,0,0,0,0,0),
 (0,0,0,0,0,0,0)
);

}

end.

