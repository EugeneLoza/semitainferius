//No parameters
procedure TMap.MapCircles(out Floor, Wall: TMapType);
var
  BiomeFloor, BiomeWall: Integer;
begin
  Log(CurrentRoutine, 'Generating map...');
  BiomeFloor := RandomFloorBiome;
  BiomeWall := RandomWallBiome;

  Floor := FillMap(SizeX, SizeY, BiomeFloor);
  Wall := FillMap(SizeX, SizeY, -1);
  MakeBorder(Wall, BiomeWall);

  repeat
    MapCircle(Wall, RND.Random(SizeX), RND.Random(SizeY), 2 + Sqr(Sqr(Sqr(RND.Random))) * (SizeX + SizeY) / 15, BiomeWall);
  until CalculateNegativeUnits(Wall) < MapArea * 0.4;
end;

