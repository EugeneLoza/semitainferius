//No parameters
procedure TMap.MapAntiCircles(out Floor, Wall: TMapType);
var
  BiomeFloor, BiomeWall: Integer;
begin
  Log(CurrentRoutine, 'Generating map...');
  BiomeFloor := RandomFloorBiome;
  BiomeWall := RandomWallBiome;

  Floor := FillMap(SizeX, SizeY, BiomeFloor);
  Wall := FillMap(SizeX, SizeY, BiomeWall);
  repeat
    MapCircle(Wall, RND.Random(SizeX), RND.Random(SizeY), 1 + RND.Random {+ Sqr(Sqr(Sqr(RND.Random))) * (SizeX + SizeY) / 15}, -1);
  until CalculateNegativeUnits(Wall) > MapArea * 0.5;
end;

