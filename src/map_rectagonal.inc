//No parameters
procedure TMap.MapRectagonal(out Floor, Wall: TMapType);
var
  x1, y1, x2, y2: Integer;
  BiomeFloor, BiomeWall: Integer;
  MinLength, MaxLength, L: Integer;
begin
  Log(CurrentRoutine, 'Generating map...');
  BiomeFloor := RandomFloorBiome;
  BiomeWall := RandomWallBiome;

  MinLength := 3;
  MaxLength := (SizeX + SizeY) div 4;

  Floor := FillMap(SizeX, SizeY, BiomeFloor);
  Wall := FillMap(SizeX, SizeY, BiomeWall);

  repeat
    x1 := RND.Random(SizeX);
    y1 := RND.Random(SizeY);
    x2 := x1;
    y2 := y1;
    L := Round(MinLength + Sqr(Sqr(Sqr(RND.Random))) * MaxLength);
    case RND.Random(4) of
      0: x2 := x1 - L;
      1: x2 := x1 + L;
      2: y2 := y1 - L;
      3: y2 := y1 + L;
    end;
    MapLine(Wall, x1, y1, x2, y2, -1);
  until CalculateNegativeUnits(Wall) > MapArea * 0.4;
end;

