# SemitaInferius

A try to revive ProjectHelena as a dungeon crawler using Castle Game Engine

# Linux

Libraries required for the game (Linux: Debian/Ubuntu package reference):

* libopenal1
* libpng
* zlib1g
* libvorbis
* libfreetype6
* libgtkglext1
* libatk-adaptor (soft dependency of GTK2)
* You'll also need OpenGL drivers for your videocard. Usually it is libgl1-mesa-dev.
* You also need X-system and GTK at least version 2, however, you are very likely to have those already installed :)

# Links

Source code repository: https://gitlab.com/EugeneLoza/semitainferius

Please, report any bugs, issues and ideas to: https://gitlab.com/EugeneLoza/semitainferius/issues